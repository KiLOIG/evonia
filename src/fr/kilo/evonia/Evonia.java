package fr.kilo.evonia;

import fr.kilo.evonia.managers.EventsManager;
import fr.kilo.evonia.utils.CheatType;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

public class Evonia extends JavaPlugin {

    /**
     *       THIS PLUGIN CONTAIN A SWITCH, REMOVING IT MADE THE SERVER AND THE PLUGIN
     *                                  COMPLETLY UNSTABLE
     */

    private static Evonia instance;

    public static ArrayList<Player> arrowFired = new ArrayList<>();

    @Override
    public void onEnable() {
        instance = this;
        System.out.println("Activation de Evonia");
        for(CheatType cheatType : CheatType.values()){
            System.out.println("Chargement du module " + cheatType.getBaseName());
        }
        new EventsManager().register(this);
    }

    @Override
    public void onDisable() {
    }

    public static Evonia getInstance(){
        return instance;
    }

}

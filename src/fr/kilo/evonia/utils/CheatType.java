package fr.kilo.evonia.utils;

public enum CheatType {

    GLIDE(1, "GLIDE", "Glide", 30),
    NOFALL(2, "NOFALL", "NoFall", 30),
    FLIGHT(3, "FLIGHT", "Fly", 30),
    SPEED(4, "SPEED", "SpeedHack", 30),
    KILLAURA(5, "KILLAURA", "KillAura", 30),
    WATERWALK(6, "WATERWALK", "WaterWalk/Jesus", 30),
    NOSLOWDOWN(7, "NOSLOWDOWN", "NoSlowDown", 30),
    FASTBOW(8, "FASTBOW", "FastBow", 30),
    BEDFUCKER(9, "BEDFUCKER", "BedFucker", 30);

    private int id;
    private String baseName;
    private String simplifiedName;
    private int banTimeInDay;

    private CheatType(int id, String baseName, String simplifiedName, int banTimeInDay){
        this.id = id;
        this.baseName = baseName;
        this.simplifiedName = simplifiedName;
        this.banTimeInDay = banTimeInDay;
    }

    public int getId() {
        return id;
    }

    public String getBaseName() {
        return baseName;
    }

    public String getSimplifiedName() {
        return simplifiedName;
    }

    public int getBanTimeInDay() {
        return banTimeInDay;
    }
}

package fr.kilo.evonia.checks;

import fr.kilo.evonia.utils.CheatType;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

public class Flight implements Listener {

    @EventHandler
    public void onMove(PlayerMoveEvent event){
        Player player = event.getPlayer();
        Location to = event.getTo();
        Location from = event.getFrom();
        Vector vec = to.toVector();
        double i = vec.distance(from.toVector());
        if(player.getLocation().getBlock().getRelative(BlockFace.DOWN).getType().equals(Material.SPONGE)){
            return;
        }
        if(player.getGameMode().equals(GameMode.CREATIVE)){
            return;
        }
        if(player.getEntityId() == 100){
            return;
        }
        if(player.getVehicle() != null){
            return;
        }
        if(player.getAllowFlight() == true){
            return;
        }
        if((player.getFallDistance() == 0.0F) && (player.getLocation().getBlock().getRelative(BlockFace.UP).getType().equals(Material.AIR))){
            if(i > 0.60D){
                if(player.isOnGround()){
                    return;
                }
                ban(player, CheatType.FLIGHT);
            }
        }
        if((i > 0.28D) && (i < 0.29D)){
            if(player.getLocation().getBlock().getRelative(BlockFace.DOWN).getType() == Material.WATER){
                return;
            }
            if(player.getLocation().getBlock().getRelative(BlockFace.DOWN).isLiquid()){
                ban(player, CheatType.WATERWALK);
            }

        }
    }

    public void ban(Player player, CheatType cheatType){
        player.kickPlayer("§6§m------------§e§m------§6§m------------\n§6ZenoriaMC §7| §cBannissement\n\n§fSi vous souhaitez voler, nous vous recommandons notre toute\n§fnouvelle pégase volante (pensez-y la prochaine fois)\n§4(" + cheatType.getSimplifiedName() + ")\n§6§m------------§e§m------§6§m------------");
    }
}

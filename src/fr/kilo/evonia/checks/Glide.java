package fr.kilo.evonia.checks;

import fr.kilo.evonia.utils.CheatType;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class Glide implements Listener {

    @EventHandler
    public void onMove(PlayerMoveEvent event){
        if((event.getTo().getY() - event.getFrom().getY() == -0.125D) && event.getTo().clone().subtract(0.0D, 1.0D, 0.0D).getBlock().getType().equals(Material.AIR)){
            Player player = event.getPlayer();
            ban(player, CheatType.GLIDE);
        }
    }

    public void ban(Player player, CheatType cheatType){
        player.kickPlayer("§6§m------------§e§m------§6§m------------\n§6ZenoriaMC §7| §cBannissement\n\n§fComme dit Denis Brognard à la fin il n'en restera qu'un\n§fIl semblerait que ce ne soit pas vous...\n§4(" + cheatType.getSimplifiedName() + ")\n§6§m------------§e§m------§6§m------------");
    }

}

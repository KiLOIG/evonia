package fr.kilo.evonia.checks;

import fr.kilo.evonia.utils.CheatType;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

public class Speed implements Listener {

    @EventHandler
    public void onMove(PlayerMoveEvent event){
        Player player = event.getPlayer();
        Location to = event.getTo();
        Location from = event.getFrom();
        Vector vec = to.toVector().setY(0.0D);
        double i = vec.distance(from.toVector().setY(0.0D));
        if(player.getLocation().getBlock().getRelative(BlockFace.DOWN).getType().equals(Material.SPONGE)){
            return;
        }
        if(player.getGameMode().equals(GameMode.CREATIVE)){
            return;
        }
        if(player.getEntityId() == 100){
            return;
        }
        if(player.getVehicle() != null){
            return;
        }
        if(player.getAllowFlight() == true){
            return;
        }
        if(from.getY() < to.getY()){
            return;
        }
        if(i >= 0.42D){
            ban(player, CheatType.SPEED);
        }
    }

    public void ban(Player player, CheatType cheatType){
        player.kickPlayer("§6§m------------§e§m------§6§m------------\n§6ZenoriaMC §7| §cBannissement\n\n§fRien ne sert de courir, il faut partir à point.\n§f- Jean de la Fontaine\n§4(" + cheatType.getSimplifiedName() + ")\n§6§m------------§e§m------§6§m------------");
    }
}

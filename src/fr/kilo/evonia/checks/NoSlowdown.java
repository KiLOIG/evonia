package fr.kilo.evonia.checks;

import fr.kilo.evonia.utils.CheatType;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

import java.util.ArrayList;

public class NoSlowdown implements Listener {

    static ArrayList<Player> bowspammed = new ArrayList<>();

    @EventHandler
    public void onMove(PlayerMoveEvent event){
        Player player = event.getPlayer();
        if(!bowspammed.contains(player)){
            return;
        }
        if(!player.isOnGround()){
            return;
        }
        Location from = event.getFrom();
        Location to = event.getTo();
        if(event.getFrom().getY() != event.getTo().getY()){
            return;
        }
        Vector vec = to.toVector();
        double dis = vec.distance(from.toVector());
        if(dis > 0.15D){
            ban(player, CheatType.NOSLOWDOWN);
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event){
        Player player = event.getPlayer();
        if(player.getItemInHand() != null){
            if(player.getItemInHand().getType().equals(Material.BOW)){
                if(event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK){
                    if(player.getInventory().contains(Material.ARROW)){
                        bowspammed.add(player);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onBowShot(EntityShootBowEvent event){
        if(event.getEntity() instanceof Player){
            Player player = (Player) event.getEntity();
            bowspammed.remove(player);
        }
    }

    @EventHandler
    public void onSwitch(PlayerItemHeldEvent event){
        Player player = event.getPlayer();
        if(bowspammed.contains(player)){
            bowspammed.remove(player);
        }
    }

    public void ban(Player player, CheatType cheatType){
        player.kickPlayer("§6§m------------§e§m------§6§m------------\n§6ZenoriaMC §7| §cBannissement\n\n§fLe saviez-vous ?\n§f90% des cheats se font détecter par Evonia\n§4(" + cheatType.getSimplifiedName() + ")\n§6§m------------§e§m------§6§m------------");
    }

}

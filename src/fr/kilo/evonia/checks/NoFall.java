package fr.kilo.evonia.checks;

import fr.kilo.evonia.utils.CheatType;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

public class NoFall implements Listener {

    @EventHandler
    public void onMove(PlayerMoveEvent event){
        Player player = event.getPlayer();
        Location from = event.getFrom().clone();
        Location to = event.getTo().clone();
        Vector vec = to.toVector();
        double i = vec.distance(from.toVector());
        if(i == 0.0D){
            return;
        }
        if(player.getGameMode().equals(GameMode.CREATIVE)){
            return;
        }
        if(player.getVehicle() != null){
            return;
        }
        if(from.getY() < to.getY()){
            return;
        }
        if((player.getFallDistance() == 0.0F) && (i > 0.79D) && (player.isOnGround())){
            ban(player, CheatType.NOFALL);
        }
    }

    public void ban(Player player, CheatType cheatType){
        player.kickPlayer("§6§m------------§e§m------§6§m------------\n§6ZenoriaMC §7| §cBannissement\n\n§fDésolé mais notre notre nouveau modèle de bottes\n§fFeather Falling 5000 n'est pas encore sortie\n§4"  + cheatType.getSimplifiedName() + "\n\n§6§m------------§e§m------§6§m------------");
    }

}

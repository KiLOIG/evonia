package fr.kilo.evonia.checks;

import fr.kilo.evonia.Evonia;
import fr.kilo.evonia.utils.CheatType;
import org.apache.logging.log4j.core.lookup.SystemPropertiesLookup;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityShootBowEvent;

import java.util.HashMap;

public class FastBow implements Listener {

    static HashMap<Player, Long> lastbow = new HashMap<>();

    @EventHandler
    public void onShoot(EntityShootBowEvent event){
        if(!(event.getEntity() instanceof Player)){
            return;
        }

        Player player = (Player) event.getEntity();
        Evonia.arrowFired.add(player);
        Bukkit.getScheduler().scheduleSyncDelayedTask(Evonia.getInstance(), new Runnable(){

            @Override
            public void run() {
                Evonia.arrowFired.remove(player);

            }
        }, 7L);
        if(!lastbow.containsKey(player)){
            lastbow.put(player, 0L);
        }

        if(event.getForce() != 1.0D){
            return;
        }

        if(lastbow.get(player) == 0L){
            lastbow.put(player, System.currentTimeMillis());
            return;
        }

        if(System.currentTimeMillis() - lastbow.get(player) < 500L){
            event.getProjectile().remove();
            ban(player, CheatType.FASTBOW);
        }

    }

    public void ban(Player player, CheatType cheatType){
        player.kickPlayer("§6§m------------§e§m------§6§m------------\n§6ZenoriaMC §7| §cBannissement\n\n§fLes arcs ne sont pas des mitraillettes\nJe répète les arcs ne sont pas des mitraillettes. Abandonnez la mission\n§4(" + cheatType.getSimplifiedName() + ")\n§6§m------------§e§m------§6§m------------");
    }
}

package fr.kilo.evonia.managers;

import fr.kilo.evonia.Evonia;
import fr.kilo.evonia.checks.*;
import org.bukkit.plugin.PluginManager;

public class EventsManager {

    public void register(Evonia evonia){
        PluginManager pm = evonia.getServer().getPluginManager();
        pm.registerEvents(new Flight(), evonia);
        pm.registerEvents(new Glide(), evonia);
        pm.registerEvents(new NoFall(), evonia);
        pm.registerEvents(new FastBow(), evonia);
        pm.registerEvents(new Speed(), evonia);
        //pm.registerEvents(new NoSlowdown(), evonia);
    }
}
